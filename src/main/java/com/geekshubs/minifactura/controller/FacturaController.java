package com.geekshubs.minifactura.controller;

import com.geekshubs.minifactura.entity.Cliente;
import com.geekshubs.minifactura.entity.Factura;
import com.geekshubs.minifactura.repository.FacturaRepository;
import com.geekshubs.minifactura.service.ClienteErrorException;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
//import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/v1")
public class FacturaController {

    @Autowired
    FacturaRepository facturaR;

    @GetMapping("/facturas")
    public List<Factura> listar(){
        System.out.println("PETICION DE LISTADO DE FACTURAS");
        return facturaR.findAll();
    }

    @GetMapping("/facturaDetalle/{id}")
    public Factura detalle(@PathVariable String id){
        System.out.println("PETICION DE DETALLE DE FACTURA");
        Factura factura=facturaR.findById(Long.parseLong(id)).orElse(null);
        //Link link=linkTo(FacturaController.class).withRel("prueba");
        //Link link=linkTo(methodOn(FacturaController.class).detalle(String.valueOf(factura.getId()))).withSelfRel();
        //factura.add(link);
        return factura;
    }
}
