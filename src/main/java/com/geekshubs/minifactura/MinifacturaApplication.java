package com.geekshubs.minifactura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@SpringBootApplication
public class MinifacturaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinifacturaApplication.class, args);
    }


}
